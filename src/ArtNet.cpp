#include "ArtNet.h"

namespace {
    WiFiUDP udpSocket;
    // AsyncUDP asyncUdpSocket;

    BoolParam enableArtnetParam;
    IntParam startUniverseParam;
    IPAddress ipAddress;
    byte packetBuffer[ARTNET_BUFFER_SIZE];

    //total universe count
    uint16_t universeCount;

    uint16_t currentSequence;

    bool doShow;
    artnet_reply_s artPollReply;
    void replyArtnetPoll();
    unsigned long artSyncTimeout;
    // CRGB artnetBuffer[NUM_LEDS];
    char nodeName[32];
    // artnet_reply_s ArtPollReply;
    // reply to art polL message
    void replyArtnetPoll() {
        artPollReply.ip[0] = ipAddress[0];
        artPollReply.ip[1] = ipAddress[1];
        artPollReply.ip[2] = ipAddress[2];
        artPollReply.ip[3] = ipAddress[3];
        // sprintf((char *)id, "Art-Net\0");
        memcpy(artPollReply.id, ART_NET_ID, sizeof(artPollReply.id));
        // memcpy(artPollReply.ip, local_ip, sizeof(artPollReply.ip));

        artPollReply.opCode = ART_POLL_REPLY;
        artPollReply.port =  ARTNET_PORT;

        memset(artPollReply.goodinput,  0x08, 4);
        memset(artPollReply.goodoutput,  0x80, 4);
        memset(artPollReply.porttypes,  0xc0, 4);

        uint8_t shortname [18];
        uint8_t longname [64];

        sprintf((char *)shortname, "wt32-driver%s\0", nodeName);
        sprintf((char *)longname, "wt32-eth01 based LED controller v:%.3f\0", 0.01);
        memcpy(artPollReply.shortname, shortname, sizeof(shortname));
        memcpy(artPollReply.longname, longname, sizeof(longname));

        artPollReply.etsaman[0] = 0;
        artPollReply.etsaman[1] = 0;
        artPollReply.verH       = 1;
        artPollReply.ver        = 0;
        artPollReply.subH       = 0;
        artPollReply.sub        = 0;
        artPollReply.oemH       = 0;
        artPollReply.oem        = 0xFF;
        artPollReply.ubea       = 0;
        artPollReply.status     = 0xd2;
        artPollReply.swvideo    = 0;
        artPollReply.swmacro    = 0;
        artPollReply.swremote   = 0;
        artPollReply.style      = 0;

        // maybe this should be reviewed
        artPollReply.numbportsH = 0;
        artPollReply.numbports  = universeCount; // can be set by LED_count.
        artPollReply.status2    = 0x08;

        artPollReply.bindip[0] = ipAddress[0];
        artPollReply.bindip[1] = ipAddress[1];
        artPollReply.bindip[2] = ipAddress[2];
        artPollReply.bindip[3] = ipAddress[3];

        uint8_t swin[4]  = {0x01,0x02,0x03,0x04};
        uint8_t swout[4] = {0x01,0x02,0x03,0x04};
        for(uint8_t i = 0; i < 4; i++)
        {
            artPollReply.swout[i] = swout[i];
            artPollReply.swin[i] = swin[i];
        }
        sprintf((char *)artPollReply.nodereport, "%i DMX output universes active.\0", artPollReply.numbports);
        udpSocket.beginPacket(udpSocket.remoteIP(), ARTNET_PORT);//send the packet to the broadcast address
        udpSocket.write((uint8_t *)&artPollReply, sizeof(artPollReply));
        udpSocket.endPacket();
    }
}


void artnetSetNodeName(const char * _str){
    strcpy(nodeName, _str);
}

// a first init, mostly to deal with parameters
void artnetInit(ParamCollector * _collector){
    enableArtnetParam.set("/artnet/enable", 1);
    enableArtnetParam.saveType = SAVE_ON_REQUEST;
    startUniverseParam.set("/artnet/universe", 1, 255, 1);
    startUniverseParam.saveType = SAVE_ON_REQUEST;
    startUniverseParam.inputType = NUMBER;

    _collector->add(&enableArtnetParam);
    _collector->add(&startUniverseParam);
}




// void receivePacket(uint8_t * _data, size_t _len){
//     // int packetSize = udpSocket.parsePacket();
//     if (_len <= ARTNET_BUFFER_SIZE && _len > 0) {
//         // dunno if we should clear the buffer first
//         memcpy(packetBuffer, _data, _len);
//         // quick check for header
//         if(memcmp(packetBuffer, ART_NET_ID, sizeof(ART_NET_ID)) != 0){
//             return;
//         }
//         // for (byte i = 0 ; i < 8 ; i++) {
//         //     if (packetBuffer[i] != ART_NET_ID[i]) {
//         //         return;
//         //     }
//         // }
//         uint16_t opcode = packetBuffer[8] | packetBuffer[9] << 8;
//         // oled.println(opcode, HEX);
//         switch(opcode) {
//             case ART_POLL:
//                 replyArtnetPoll();
//                 break;
//             case ART_DMX:
//                 {
//                     uint16_t seq = packetBuffer[12];
//                     uint16_t uni = packetBuffer[14] | packetBuffer[15] << 8;
//                     uint16_t len = packetBuffer[17] | packetBuffer[16] << 8;
//                     Serial.printf("uni:%02i len:%03i seq:%03i\n",uni,len,seq);
//                     // receiveData(uni, len, seq, packetBuffer + ART_DMX_START);
//                     if(uni >= startUniverseParam.v-1){
//                         if(uni < startUniverseParam.v-1 + universeCount){
//                             uni -= startUniverseParam.v-1;
//                             // memcpy(_leds + uni * (512/3), packetBuffer + ART_DMX_START, len);

//                 // #ifdef RGBW_MODE
//                 //             // uni is not devided by 4 due to drawingMemory vs leds
//                 //             memcpy(drawingMemory+uni*512, _data, _len);
//                 // #else
//                             // memcpy(artnetBuffer+ uni * (512/3), _data, _len);
//                 // #endif
//                         }
//                     }
//                     // if we dont receive artsync packet for 4 seconds
//                     // we switch to non-Synchronous mode
//                     if(artSyncTimeout+4000 < millis()){
//                         if(seq != currentSequence){
//                             currentSequence = seq;
//                             FastLED.show();
//                         }
//                     }       
//                 }
//                 break;
//             case ART_SYNC:
//                 Serial.println("--------");
//                 artSyncTimeout = millis();
//                 FastLED.show();
//                 break;
//         }
//     }
// }




// the real begin function that should be called once the network and stuff is started
void artnetBegin(){
    // if(enableArtnetParam.v){
    udpSocket.begin(ARTNET_PORT);
    artSyncTimeout = 0;
    // if(asyncUdpSocket.listen(ARTNET_PORT)){
    //     asyncUdpSocket.onPacket([](AsyncUDPPacket packet){
    //             receivePacket(packet.data(), packet.length());
    //         }
    //     );
    // }

    // }
}


void receiveData(uint16_t _uni, uint16_t _len, uint16_t _seq, uint8_t * _data){
//     if(_uni >= startUniverseParam.v-1){
//         if(_uni < startUniverseParam.v-1 + universeCount){
//             _uni -= startUniverseParam.v-1;
// // #ifdef RGBW_MODE
// //             // _uni is not devided by 4 due to drawingMemory vs leds
// //             memcpy(drawingMemory+uni*512, _data, _len);
// // #else
//             memcpy(artnetBuffer+ _uni * (512/3), _data, _len);
// // #endif
//         }
//     }
}

// void artnetOutputLedData(){
//     if(decimatorParam.v > 1){
//         for(int i = NUM_LEDS; i >= 0; i--) {
//             leds[i] = artnetBuffer[i/decimatorParam.v];
//         }
//     }
//     else {
//         memcpy(leds, artnetBuffer, sizeof(leds));
//     }
// #ifdef LEDRIVER_USE_RGBW
//     showLEDsOcto();
// #else
//     showLEDs();
// #endif
// }

// exposed loop it
void artnetUpdate(CRGB * _leds, size_t _count){
    if(enableArtnetParam.v){
        int packetSize = udpSocket.parsePacket();
        if (packetSize <= ARTNET_BUFFER_SIZE && packetSize > 0) {
            // dunno if we should clear the buffer first
            udpSocket.read(packetBuffer, packetSize);
            // quick check for header
            if(memcmp(packetBuffer, ART_NET_ID, sizeof(ART_NET_ID)) != 0){
                return;
            }
            // for (byte i = 0 ; i < 8 ; i++) {
            //     if (packetBuffer[i] != ART_NET_ID[i]) {
            //         return;
            //     }
            // }
            uint16_t opcode = packetBuffer[8] | packetBuffer[9] << 8;
            // oled.println(opcode, HEX);
            switch(opcode) {
                case ART_POLL:
                    replyArtnetPoll();
                    break;
                case ART_DMX:
                    {
                        uint16_t seq = packetBuffer[12];
                        uint16_t uni = packetBuffer[14] | packetBuffer[15] << 8;
                        uint16_t len = packetBuffer[17] | packetBuffer[16] << 8;
                        Serial.printf("uni:%02i len:%03i seq:%03i\n",uni,len,seq);
                        // receiveData(uni, len, seq, packetBuffer + ART_DMX_START);
                        if(uni >= startUniverseParam.v-1){
                            if(uni < startUniverseParam.v-1 + universeCount){
                                uni -= startUniverseParam.v-1;
                                memcpy(_leds + uni * (512/3), packetBuffer + ART_DMX_START, len);

                    // #ifdef RGBW_MODE
                    //             // uni is not devided by 4 due to drawingMemory vs leds
                    //             memcpy(drawingMemory+uni*512, _data, _len);
                    // #else
                                // memcpy(artnetBuffer+ uni * (512/3), _data, _len);
                    // #endif
                            }
                        }
                        // if we dont receive artsync packet for 4 seconds
                        // we switch to non-Synchronous mode
                        if(artSyncTimeout+4000 < millis()){
                            if(seq != currentSequence){
                                currentSequence = seq;
                                FastLED.show();
                            }
                        }       
                    }
                    break;
                case ART_SYNC:
                    Serial.println("--------");
                    artSyncTimeout = millis();
                    FastLED.show();
                    break;
            }
        }
    }
}

// void sendArtnetPacket(
//     uint8_t * _data, 
//     uint16_t _size, 
//     uint8_t _uni,
//     uint8_t _seq,
//     IPAddress _dest
// ){
//     static uint8_t buffer;
//     memset(packetBuffer, 0, sizeof(packetBuffer));

//     packetBuffer[0] = 'A';
//     packetBuffer[1] = 'r';
//     packetBuffer[2] = 't';
//     packetBuffer[3] = '-';
//     packetBuffer[4] = 'N';
//     packetBuffer[5] = 'e';
//     packetBuffer[6] = 't';
//     packetBuffer[7] = 0; // null terminated string
//     packetBuffer[8] = 0; //opcode
//     packetBuffer[9] = 80; //opcode
//     packetBuffer[10] = 0; //protocol version
//     packetBuffer[11] = 14; //protocol version
//     packetBuffer[12] = _seq; //sequence
//     packetBuffer[13] = 0; //physical (purely informative)
//     packetBuffer[14] = _uni;//(byte)(_uni%16); //Universe lsb? http://www.madmapper.com/universe-decimal-to-artnet-pathport/
//     packetBuffer[15] = 0;//(byte)(_uni/16); //Universe msb?
//     packetBuffer[16] = ((_size & 0xFF00) >> 8); //length msb
//     packetBuffer[17] = (_size & 0xFF); //length lsb
    
//     memcpy(packetBuffer+18, _data, _size);
//     udpSocket.beginPacket(_dest, ARTNET_PORT);
//     udpSocket.write((uint8_t *)&packetBuffer, _size+18);
//     udpSocket.endPacket();
// }

// void sendArtSync(IPAddress _sync){
//     memset(packetBuffer, 0, sizeof(packetBuffer));
//     packetBuffer[0] = byte('A');
//     packetBuffer[1] = byte('r');
//     packetBuffer[2] = byte('t');
//     packetBuffer[3] = byte('-');
//     packetBuffer[4] = byte('N');
//     packetBuffer[5] = byte('e');
//     packetBuffer[6] = byte('t');
//     packetBuffer[7] = 0; // null terminated string
//     packetBuffer[8] = 0; //opcode
//     packetBuffer[9] = 82; //opcode
//     packetBuffer[10] = 0; //protocol version
//     packetBuffer[11] = 14; //protocol version
//     packetBuffer[12] = 0; //protocol version
//     packetBuffer[13] = 0; //protocol version
//     udpSocket.beginPacket(_sync, ARTNET_PORT);
//     udpSocket.write((uint8_t *)&packetBuffer, 14);
//     udpSocket.endPacket();
// }


// void sendDataArtnet(uint8_t * _data, size_t _size, uint8_t _uni, IPAddress _dest){
//     static uint32_t sequence = 0;
//     // chunk the data into universes;
//     int packetCount = _size/512;
//     if(packetCount == 0) packetCount = 1;
//     for(int i = 0; i < packetCount; i++){
//         sendArtnetPacket(
//             _data + i*512,
//             (i != packetCount-1) ? 512 : _size%512,
//             _uni+i,
//             sequence,
//             _dest 
//         );
//     }
//     sequence++;
// }