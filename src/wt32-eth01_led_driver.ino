
#include "wt32-eth01_param.h"
#include <HardwareSerial.h>
#include <MIDI.h>



#include <ArtNet.h>
#define SCK 0
#define MISO 0
#define MOSI 0
#define SS 0

#include "FastLED.h"

#define LED_DATA_1_PIN 2
#define LED_DATA_2_PIN 4
#define LED_DATA_3_PIN 12
#define LED_DATA_4_PIN 14

#define LED_DATA_5_PIN 15   // <--- SERIAL IN
#define LED_DATA_6_PIN 17

// #define LED_DATA_7_PIN 5
// #define LED_DATA_8_PIN 33
HardwareSerial  MidiSerial = HardwareSerial(2);

using Transport = MIDI_NAMESPACE::SerialMIDI<HardwareSerial>;
Transport serialMIDI(MidiSerial);
MIDI_NAMESPACE::MidiInterface<Transport> MIDI((Transport&)serialMIDI);


#define STATUS_LED_PIN 32
#define INPUT_A_PIN    36
#define INPUT_B_PIN    39

#define LED_TYPE    WS2813
#define COLOR_ORDER GRB
#define NUM_STRIPS  4
#define NUM_LEDS_PER_STRIP 60
#define NUM_LEDS NUM_LEDS_PER_STRIP * NUM_STRIPS
CRGB leds[NUM_LEDS];


ColorParam colorParam;
IntParam dotmodParam;
IntParam stepParam;
BoolParam enableSolenoidsParam;

IntParam pulseLengthParam;
BangParam triggerPulseParam;

// we use a color to receive the data in one message
// IntParam sequenceMessageParam;

typedef struct {
    // led idx 
    uint8_t noteID;
    uint16_t solenoidAddress;
    // uint16_t solenoidColorOffset;
    uint16_t ledAddress;
    // uint16_t ledColorOffset;
    uint16_t pulseLength;
    CRGB col;
    long triggerStamp;
} PinPulser;

// const char noteIdAddress = "/tdlf/sole/%i/note";
// const char solenoidAddress = "/tdlf/sole/%i/pin";
// const char pulseLengthAddress = ""

// typedef struct {
//     IntParam noteIdParam;
//     IntParam solenoidIdParam;
//     IntParam pulseLengthParam;
//     BangParam triggerParam;
// } PinParams;

#define MAX_PULSE_COUNT 8
PinPulser pulses[MAX_PULSE_COUNT];
long led_anim_stamp = 0; 

#define NULL_LED_ADDRESS

void initPulses(){
    for(int i = 0 ; i < MAX_PULSE_COUNT; i++){
        pulses[i].noteID = 255;
        pulses[i].ledAddress = 8;
        pulses[i].solenoidAddress = 8;
    }
    int ledoffset = NUM_LEDS_PER_STRIP;
    pulses[0].noteID = 38; // 36
    pulses[0].pulseLength = 20;
    pulses[0].solenoidAddress = 0;
    pulses[0].ledAddress = ledoffset;
    pulses[0].triggerStamp = 0;
    pulses[0].col = CRGB(255,0,0);

    pulses[1].noteID = 43;
    pulses[1].pulseLength = 20;
    pulses[1].solenoidAddress = 1;
    pulses[1].ledAddress = ledoffset+1;
    pulses[1].triggerStamp = 0;
    pulses[1].col = CRGB(0,255,0);

    pulses[2].noteID = 50;
    pulses[2].pulseLength = 20;
    pulses[2].solenoidAddress = 2;
    pulses[2].ledAddress = ledoffset+2;
    pulses[2].triggerStamp = 0;
    pulses[2].col = CRGB(0,0,255);

    pulses[3].noteID = 42;
    pulses[3].pulseLength = 20;
    pulses[3].solenoidAddress = 3;
    pulses[3].ledAddress = ledoffset+3;
    pulses[3].triggerStamp = 0;
    pulses[3].col = CRGB(0,0,255);
}

// int solenoidToLed(int s){
//     return s/3
// }

void updateLEDs(){
    for(int i = 0; i < MAX_PULSE_COUNT; i++){
        if(pulses[i].triggerStamp + pulses[i].pulseLength  > millis() ){
            // Serial.println("pulse");
            if(enableSolenoidsParam.v){
                leds[pulses[i].solenoidAddress] = CRGB(255,255,255);
            }
            else {
                leds[pulses[i].solenoidAddress] = CRGB(0,0,0);
            }
            leds[pulses[i].ledAddress] = pulses[i].col;
        }
        else {
            leds[pulses[i].solenoidAddress] = CRGB(0,0,0);
            leds[pulses[i].ledAddress] = CRGB(0,0,0);
        }
    }

    leds[0].r = leds[0].r;
    leds[0].g = leds[1].r;
    leds[0].b = leds[2].r;
    leds[1].r = leds[3].r;
    leds[1].g = 0;
    leds[1].b = 0;

    // leds[0].r = (millis()%1000 < 30) ? 255 : 0;
    // leds[0].g = (millis()%1000 < 30) ? 255 : 0; //    leds[1].r;
    // leds[0].b = (millis()%1000 < 30) ? 255 : 0;
    // Serial.printf("%03i %03i %03i\n", leds[0].r, leds[1].r, leds[2].r);
    FastLED.show();
}



void handleNoteOn(byte channel, byte pitch, byte velocity){
    if(pitch != 0){
        if(pitch == 36) Serial.print("-----");
        Serial.printf("[midi] noteOn chan:%03i pitch:%03i vel:%03i\n",channel,pitch,velocity);
        for(int i = 0 ; i < MAX_PULSE_COUNT; i++){
            if(pitch == pulses[i].noteID){
                Serial.println(pitch);
                pulses[i].triggerStamp = millis();
            }
        }
    }
}

void handleNoteOff(byte channel, byte pitch, byte velocity){
    // Serial.printf("[midi] noteOff chan:%03i pitch:%03i vel:%03i\n",channel,pitch,velocity);
}

void setup(){
    Serial.begin(115200);
    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(STATUS_LED_PIN, HIGH);
    MidiSerial.begin(31250, SERIAL_8N1, LED_DATA_5_PIN, LED_DATA_6_PIN);

    enableSolenoidsParam.set("/solenoid/enable", true);
    paramCollector.add(&enableSolenoidsParam);


    dotmodParam.set("/anim/dotmod", 4, 300, 20);
    dotmodParam.saveType = SAVE_ON_REQUEST;

    colorParam.set("/anim/color", CRGB::Red);
    
    paramCollector.add(&colorParam);
    paramCollector.add(&dotmodParam);
    artnetInit(&paramCollector);
    
    artnetInit(&paramCollector);
    setupWt();
    // wt32Params.wifiEnableParam.setBoolValue(0);
    startNetwork();
    artnetBegin();
    FastLED.addLeds<LED_TYPE, LED_DATA_1_PIN, COLOR_ORDER>(leds, 0, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_2_PIN, COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_3_PIN, COLOR_ORDER>(leds, 2 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_4_PIN, COLOR_ORDER>(leds, 3 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    // FastLED.addLeds<LED_TYPE, LED_DATA_5_PIN, COLOR_ORDER>(leds, 4 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    // FastLED.addLeds<LED_TYPE, LED_DATA_6_PIN, COLOR_ORDER>(leds, 5 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    // FastLED.addLeds<LED_TYPE, LED_DATA_7_PIN, COLOR_ORDER>(leds, 6 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    // FastLED.addLeds<LED_TYPE, LED_DATA_8_PIN, COLOR_ORDER>(leds, 7 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
    
    artnetBegin();
    initPulses();
    digitalWrite(STATUS_LED_PIN, LOW);

    MIDI.setHandleNoteOn(handleNoteOn);
    MIDI.setHandleNoteOff(handleNoteOff);
    MIDI.begin(MIDI_CHANNEL_OMNI);
}


long stamp = 0;

void loop(){ 
    updateWt();
    MIDI.read();
    // if(MidiSerial.available()){
    //     while(MidiSerial.available()){
    //         Serial.print(MidiSerial.read());
    //         Serial.print(' ');
    //     }
    //     Serial.println();
    // }
    updateLEDs();
    digitalWrite(STATUS_LED_PIN, millis()%250 < 100);
}





