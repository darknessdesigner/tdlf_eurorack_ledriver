/*
wt32-eth01_param.h
Boiler plate to integrate Param library on the wt32-eth01
Features :
- basic set of parameters to configure network interfaces
- param websocket for web interface
- save and load parameters from esp32 flash
- receive OSC
- aver the air flashing

@author maxd@nnvtn.ca
*/


#ifndef WT32_ETH01_PARAM_H
#define WT32_ETH01_PARAM_H

#include <wt32-eth01_config.h>

#include <WiFi.h>
#include <ETH.h>
#include <ArduinoOTA.h>
#include <SPIFFS.h>

#include <Param.h>
#include <ParamWebsockets.h>
#include <ParamOsc.h>

typedef struct wt32_params
{
    // wifi params
    BoolParam wifiEnableParam;
    BoolParam wifiDhcpParam;
    TextParam wifiIpParam;
    TextParam wifiGatewayParam;
    TextParam wifiSubnetParam;
    TextParam wifiSsidParam;
    TextParam wifiPswdParam;

    // eth params
    BoolParam ethEnableParam;
    BoolParam ethDhcpParam;
    TextParam ethIpParam;
    TextParam ethGatewayParam;
    TextParam ethSubnetParam;

    // other params
    IntParam oscInPortParam;
    // osc out, enable ota, hostname?
    BangParam saveParam;
    // to implement
    BangParam deleteConfigParam;
} wt32_params_t;

// for changing config
extern wt32_params_t wt32Params;

// we provide a global ParamCollector to collect parameters throughout the code
extern ParamCollector paramCollector;
extern WiFiUDP oscUdpSocket;
extern IPAddress oscRemoteIP;
extern bool oscInputFlag;
// namespace wt32_params;
void setupWt();
void startNetwork();
void updateWt();

void saveParamsSpiffs();

#endif